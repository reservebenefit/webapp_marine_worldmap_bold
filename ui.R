###############################################################################
## Codes for the paper:
## Global determinants of freshwater and marine fish genetic diversity
## Authors :
## Stephanie Manel, Pierre-Edouard Guerin, David Mouillot,
## Simon Blanchet, Laure Velez, Camille Albouy, Loic Pellissier
## 
## Montpellier, 2017-2019
## Nature communications, 2020
##
###############################################################################
###############################################################################
# load libraries
library(shiny)
library(leaflet)
library(htmltools)
library(htmlwidgets)
library(sf)
library(tidyverse)
library(shinythemes)
library(DT)
library(shinydashboard)
library(shinycssloaders)


###############################################################################
## DATA
choices_datatype=list("Genetic diversity" = "mean_genetic_diversity",
" Surface temperature" = "temperature",
"Number of sequenced species" = "number_of_species",
"Mean number of individual sequences per species" = "number_of_sequences")

choices_watertype=c("marine","freshwater")

## Add directory of static resources to Shiny's web server
addResourcePath(prefix = "logos", directoryPath = "docs/logos/")

###############################################################################
## UI
ui <- fluidPage( 
  
      navbarPage(title = "Worldmap of Fish Genetic Diversity",
                theme = shinytheme("flatly"),

      tabPanel(title = "Maps",fluid = TRUE, icon = icon("globe-africa"),
        sidebarLayout(
          sidebarPanel(
               
  
    		    ## Select freshwater or marine fishes
            selectInput("watertype", 
                                              label = "freshwater/marine species",
                                              choices = choices_watertype,
                                              selected = "marine"),
    
                                       
    
    
            ## Select data type
            selectInput("cell_datatype", 
                                              label = "Cell values",
                                              choices = choices_datatype,
                                              selected = "mean_genetic_diversity"),

            hr(), 
            
            tags$footer(tags$p("Actinopterygii mitochondrial barcode sequences extracted and curated from Barcode Of Life database (December 2018)"))
        
          ),
          mainPanel(
  	        fluidRow(
  		      ## the worldmap
  		      leafletOutput("mymap",height=640,width=960)
  	        )
  		    )
        )
      ),
      tabPanel("About",fluid = TRUE, icon = icon("info-circle"),
               includeMarkdown("docs/infos.md")
      )
      
  ), 
    br(),
    hr(),
    tags$a(href='https://www.cefe.cnrs.fr/fr/',
           tags$img(src='logos/cnrs_logo.png', height=60,width=60)),
    tags$a(href='https://www.ephe.psl.eu/',
         tags$img(src='logos/ephe_logo.png', height=50,width=250)),
    tags$a(href='https://ethz.ch',
         tags$img(src='logos/eth_zurich_logo.png', height=30,width=160)),
    tags$a(href='https://wwz.ifremer.fr/',
         tags$img(src='logos/ifremer_logo.png', height=50,width=120)),
)


