###############################################################################
## Codes for the paper:
## Global determinants of freshwater and marine fish genetic diversity
## Authors :
## Stephanie Manel, Pierre-Edouard Guerin, David Mouillot,
## Simon Blanchet, Laure Velez, Camille Albouy, Loic Pellissier
## 
## Montpellier, 2017-2019
## Nature communications, 2020
##
###############################################################################
###############################################################################
# load libraries
library(shiny)
library(leaflet)


library(htmlwidgets)
library(htmltools)
library(sf)
library(tidyverse)
library(shinythemes)
library(DT)
library(shinydashboard)


###############################################################################
# load data
#source("prepare_data.R")
load("donnees/cellsDat.RData")


###############################################################################
# AESTETHICS
gradiant_col_palette=c("#3333A2","#3333FF","#33CBFF","#33FFFF","#FFDF33","#FFA333","#FF3333")
bin_col_palette=c("#FFCCCC","#FF6666","#FF0000","#990000")


###############################################################################
# SERVER
r_colors <- rgb(t(col2rgb(colors()) / 255))
names(r_colors) <- colors()

server <- function(input, output, session) {

  #selected_data= marineRect %>% dplyr::select(input$cell_datatype)


  #############################################################################
  ## LEAFLET
  output$mymap <- output$map <- renderLeaflet({

    selected_wt= cellsDat %>% filter(watertype == input$watertype)
    selected_data=selected_wt %>% dplyr::select(input$cell_datatype) %>% pull()



    if(input$cell_datatype=="number_of_species") {
      maFillColor=colorBin(bin_col_palette, selected_data, bins=c(2,4,5,10, max(selected_data)) )(selected_data)
      selected_interval_data=c(2,4,5,10, max(selected_data))
      conpal=colorBin(bin_col_palette, selected_data, bins=c(2,4,5,10, max(selected_data)) )
      titre_legende="Number of sequenced species"
    } else if(input$cell_datatype =="mean_genetic_diversity") {
      bins_gd = c(0,0.001,0.002,0.003,0.005,0.01,0.025,max(selected_data))
      maFillColor= colorBin(gradiant_col_palette, selected_data, bins=bins_gd)(selected_data)
      selected_interval_data=bins_gd
      conpal = colorBin(gradiant_col_palette, selected_data, bins=bins_gd)
      titre_legende="Mean genetic diversity"  


    } else if(input$cell_datatype =="temperature") {
      maFillColor= colorNumeric(gradiant_col_palette, selected_data )(selected_data)
      selected_interval_data=c(min(selected_data),max(selected_data))
      conpal = colorNumeric(palette = gradiant_col_palette, domain = selected_interval_data)
      titre_legende="Mean surface temperature (°C)"
    } else {
      ## number of sequences by species
      maFillColor=colorBin(bin_col_palette, selected_data, bins=c(2,5,10,20,30, max(selected_data)) )(selected_data)
      selected_interval_data=c(2,5,10,20,30, max(selected_data))
      conpal=colorBin(bin_col_palette,selected_data, bins=c(2,5,10,20,30, max(selected_data)) )
      titre_legende="Mean number of available individual sequences per species"

    }
    labels <- sprintf("<strong>%s</strong><br/> %g", titre_legende, selected_data) %>% lapply(htmltools::HTML)

    leaflet() %>%
      addProviderTiles(providers$Hydda.Base,
                       options = providerTileOptions(minZoom = 2, maxZoom = 400)) %>%
      clearBounds()  %>%
      setView(lng = 20, lat = 0, zoom = 2)  %>%
      addTiles() %>%
      addRectangles(
        lng1=selected_wt$lng1, lat1=selected_wt$lat1,
        lng2=selected_wt$lng2, lat2=selected_wt$lat2,
        color="#ffffff",
        dashArray = "1",
        weight = 1, smoothFactor = 0.5,
        opacity = 1.0, fillOpacity = 0.5,
        fillColor = maFillColor,
        highlight = highlightOptions(
                    weight = 5,
                    color = "#ffffff",
                    dashArray = "",
                    fillOpacity = 1,
                    bringToFront = TRUE),
                  label = labels,
                  labelOptions = labelOptions(
                    style = list("font-weight" = "normal", padding = "2px 8px"),
                    textsize = "14px",
                    direction = "auto")
      ) %>%
       addLegend(conpal, 
                selected_interval_data,
                opacity = 1, 
                title = titre_legende,
                position = "bottomright")
  })

  #############################################################################

}

