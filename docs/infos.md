## Welcome to **W**orldmap of **F**ish **G**enetic **D**iversity (**WFGD**)

The app is designed to visualize biogeographic patterns of fish genetic diversity.
So, we mined the [Barcode Of Life database](http://www.boldsystems.org/) to extract mitochondrial barcode sequences of Cytochrome Oxidase Subunit 1 (COI) from marine and freshwater actinopterygii species.

Genetic diversity was estimated as the mean number of mutations per base pair for COI sequence across species within each cell on a worldwide grid with a 200-km spatial resolution.
* 514 cells for marine fishes
* 343 cells for freshwater fishes

The colour gradient represents the relative variation of intraspecific genetic diversity: the reddest square cells have the highest genetic diversity. 


### Method

![methodmapmarinepng](map_marine_workflow.png)



### Reference

**WFGD** was developped by **[Pierre-Edouard GUERIN](https://www.cefe.cnrs.fr/fr/recherche/bc/bev/884-it/3594-pierre-edouard-guerin)** and supports the following paper:

> **Global determinants of freshwater and marine fish genetic diversity**
>
> *Stéphanie Manel, Pierre-Edouard Guerin, David Mouillot, Simon Blanchet, Laure Velez, Camille Albouy & Loïc Pellissier*
>
> Nature communications. 2020 Feb 10. DOI: https://doi.org/10.1038/s41467-020-14409-7


The source code of the method is available at: https://gitlab.mbb.univ-montp2.fr/reservebenefit/worldmap_fish_genetic_diversity

The template is inspired by the [GAPeDNA app](https://github.com/virginiemarques/GAPeDNA") with kind help of Virginie Marques.

Last updated in February 2021
