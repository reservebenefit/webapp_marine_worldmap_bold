# WFGD : a webserver for Worldmap of Fish Genetic Diversity

Pierre-Edouard **GUERIN**

Montpellier, 2017-2019

___________________________________________________

# Installation

## Prerequisites

### CONDA

* miniconda

Instructions to install CONDA are [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html)

* CONDA environement with R and [shiny](https://anaconda.org/r/r-shiny)

```
conda env create -f environment.yml
conda activate rshiny
```

To quit conda environment:
```
conda deactivate
```

To remove this environment:
```
conda remove --name rshiny --all
```

Check CONDA environments
```
conda info --envs
```


# Display the web-app


First, download this repository.

```bash
git clone https://gitlab.mbb.univ-montp2.fr/reservebenefit/webapp_marine_worldmap_bold.git
cd webapp_marine_worldmap_bold
```

Then activate CONDA environment

```bash
## create the environment the first time
conda env create -f environment.yml
## activate rshiny environment to get R with all required dependencies
conda activate rshiny
```

Finally, run a R session

```bash
R
```

Inside your R session, load `shiny` and then, run the web-app in local

```R
## load shiny library
library('shiny')
## define web browser to use
options(browser="/usr/bin/firefox")
## run shiny web app
runApp(launch.browser=TRUE)
```